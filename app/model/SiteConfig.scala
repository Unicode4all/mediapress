package model

import play.api.Play

object SiteConfig {
  private var _name: String = null
  private var _baseUrl: String = null

  def name = _name
  def name_=(value: String): Unit = _name = value

  def baseUrl = _baseUrl
  def baseUrl_=(value: String): Unit = _baseUrl = value
  
  def update = {
    baseUrl_=(Play.current.configuration.getString("site.baseurl").get)
    name_=(Play.current.configuration.getString("site.name").get)
  }
}